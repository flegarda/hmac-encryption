<?php

namespace hmac\Services;
use \Firebase\JWT\JWT;

class Signature  
{
	 var $_secret_key = "";
	 var $_hmac_algo = "";
	 var $_ttl       = "";
	 var $_client_signature  = "";
	 var $_max_interval  = "";
     
    /**
     * @param string      $jwt    The JWT
     * @param string|null $key    The secret key
     * @param bool        $verify Don't skip verification process
     *
     * @return object The JWT's payload as a PHP object
     */
     
   public function __construct($secretKey,$ttl,$hmac_algo,$max_interval){
	    $this->_secret_key   = $secretKey;
	    $this->_hmac_algo    = $hmac_algo;
	    $this->_ttl          = $ttl;
	    $this->_max_interval = $max_interval;
     }
     
   

    public  function validateSignature($payload = array()){
	     $this->_client_signature = $this->getAuthorizationHeader(); /*get client signature*/
		 $server_signature        = $this->payload_to_signature($payload);
		 $call_interval           = $this->validateTimeInterval($payload['iat']);

		if($call_interval===false)
		     throw new \Error\Exception\UserNotActive("Unauthorized");
		elseif($this->_client_signature  !==  $server_signature or empty($this->_client_signature))
		     throw new \Error\Exception\UserNotActive("Unauthorized");
		else
		   return true;
	}
	
	private function validateTimeInterval($client_ait){
		
	  $date_utc = new \DateTime("now", new \DateTimeZone("UTC"));
      $exec_at = $date_utc->format('Y-m-d H:i:s').' UTC'; 
      if($this->_max_interval <= (strtotime($exec_at)  - strtotime($client_ait)))
          return false;
      else
          return true;
	}
	
    private  function payload_to_signature($posted_payload){ 
		$payload = json_encode($posted_payload);
		// Encode Payload to Base64Url String
		$base64_url_payload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
		/*
		 * This is the key which will be known to client and server and will be used for hashing the message
		 * Server will use this key to decode the message and verify the authenticity of the client calls
		 */
		$shared_secret_key =  $this->_secret_key ; // Use shared secret key here, which should be stored in config file
		$signature = hash_hmac( $this->_hmac_algo,  $base64_url_payload, $shared_secret_key, true);
		// Encode Signature to Base64Url String
		$base64_url_signature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));
		return  JWT::encode($base64_url_signature,$shared_secret_key);
    }
    
    public function  getAuthorizationHeader(){
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }
        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }
}
